package com.selfstudy.pattern.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.UserModel;
import com.selfstudy.pattern.service.IUserServiceAbstract;
import com.selfstudy.pattern.service.IUserServiceConcrete;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private IUserServiceConcrete serviceUserConcrete;
	
	@Autowired
	private IUserServiceAbstract serviceUserAbstract;
	
	@GetMapping("/all_dto_concrete")
	public ResponseEntity<List<UserDto>> showAllUser() {
		return ResponseEntity.status(HttpStatus.OK).body(serviceUserConcrete.getAllUserDto());
	}
	
	@GetMapping("/all_model_concrete") 
	public ResponseEntity<List<UserModel>> showAllUserModel() {
		return ResponseEntity.status(HttpStatus.OK).body(serviceUserConcrete.getAllUserModel());
	}
	
	
	@GetMapping("/specific")
	public ResponseEntity<UserDto> getUserDto() {
		return ResponseEntity.status(HttpStatus.OK).body(serviceUserConcrete.getUserDtoConcrete());
	}
	
	@GetMapping("/abstract_list_dto") // sirve 
	public ResponseEntity<List<UserDto>> getUserDtoAbstract () {
		return ResponseEntity.status(HttpStatus.OK).body(serviceUserAbstract.getAllUserDto());
	}
	
	@GetMapping("/all_model_abstract")
	public ResponseEntity<List<UserModel>> showAllUserModelAbstract() {
		return ResponseEntity.status(HttpStatus.OK).body(serviceUserAbstract.getAllUserModel());
	}
	
}
