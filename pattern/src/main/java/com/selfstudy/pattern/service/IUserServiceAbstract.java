package com.selfstudy.pattern.service;

import java.util.List;

import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.UserModel;

public interface IUserServiceAbstract {

	List<UserDto> getAllUserDto();

	List<UserModel> getAllUserModel();
}
