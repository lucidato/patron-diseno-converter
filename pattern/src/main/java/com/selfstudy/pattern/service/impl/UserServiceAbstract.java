package com.selfstudy.pattern.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.selfstudy.pattern.converter.UserConverterAbstract;
import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.RolModel;
import com.selfstudy.pattern.model.UserModel;
import com.selfstudy.pattern.service.IUserServiceAbstract;

@Service
public class UserServiceAbstract implements IUserServiceAbstract {
	
	private final UserConverterAbstract userConverter = new UserConverterAbstract();
	 
	private List<UserModel> getListUserModel() {

		// user 1
		UserModel userModel1 = UserModel.builder().firstName("Mary").isActive(true).lastName("Perez").userId("34567")
				.build();
		RolModel rol1 = RolModel.builder().rolId("1").description("Rol 1").user(userModel1).build();
		RolModel rol2 = RolModel.builder().rolId("2").description("Rol 2").user(userModel1).build();
		List<RolModel> rolList = new ArrayList<>();
		rolList.add(rol1);
		rolList.add(rol2);
		userModel1.setRolList(rolList);

		// user 2

		UserModel userModel2 = UserModel.builder().firstName("Peter").isActive(false).lastName("Fake").userId("frrgd")
				.build();

		RolModel rol3 = RolModel.builder().rolId("3").description("Rol 3").user(userModel2).build();
		RolModel rol4 = RolModel.builder().rolId("4").description("Rol 4").user(userModel2).build();
		RolModel rol5 = RolModel.builder().rolId("55").description("Rol 5").user(userModel2).build();
		List<RolModel> rolList2 = new ArrayList<>();
		rolList2.add(rol3);
		rolList2.add(rol4);
		rolList2.add(rol5);
		userModel2.setRolList(rolList2);

		// user 3
		UserModel userModel3 = UserModel.builder().firstName("undefined").isActive(true).lastName("undefine")
				.userId(null).build();

		RolModel rol6 = RolModel.builder().rolId("6").description("Rol 6").user(userModel3).build();
		List<RolModel> rolList3 = new ArrayList<>();
		rolList3.add(rol6);
		userModel3.setRolList(rolList3);

		List<UserModel> listModelUser = new ArrayList<>();
		listModelUser.add(userModel1);
		listModelUser.add(userModel2);
		listModelUser.add(userModel3);

		return listModelUser;
	}

	@Override
	public List<UserDto> getAllUserDto() {

		List<UserModel> listModel = getListUserModel(); // aca iria la consulta jpa
		return userConverter.fromEntity(listModel);
	}

	@Override
	public List<UserModel> getAllUserModel() {
		return getListUserModel();
	}

}
