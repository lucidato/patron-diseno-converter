package com.selfstudy.pattern.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.selfstudy.pattern.converter.ConverterConcrete;
import com.selfstudy.pattern.converter.UserConverterConcrete;
import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.RolModel;
import com.selfstudy.pattern.model.UserModel;
import com.selfstudy.pattern.service.IUserServiceConcrete;

@Service
public class UserServiceConcrete implements IUserServiceConcrete {

	@Override
	public UserDto getUserDtoConcrete() {

		ConverterConcrete<UserDto, UserModel> userConverter = new UserConverterConcrete();

		UserModel userModel = UserModel.builder().firstName("Mary").isActive(true).lastName("Perez").userId("34567")
				.build();

		return userConverter.convertFromEntity(userModel);
	}

	@Override
	public List<UserDto> getAllUserDto() {
		
		ConverterConcrete<UserDto, UserModel> userConverter = new UserConverterConcrete();

		List<UserModel> listModelUser = getListUserModel();

		return userConverter.createFromEntities(listModelUser);
	}

	/**
	 * 
	 * @return
	 */
	private List<UserModel> getListUserModel() {
		
		UserModel userModel1 = UserModel.builder().firstName("Mary").isActive(true).lastName("Perez").userId("34567")
				.build();
		RolModel rol1 = RolModel.builder().rolId("1").description("Rol 1").user(userModel1).build();
		RolModel rol2 = RolModel.builder().rolId("2").description("Rol 2").user(userModel1).build();
		List<RolModel> rolList = new  ArrayList<>();
		rolList.add(rol1);
		rolList.add(rol2);
		userModel1.setRolList(rolList);
		
		//
		
		UserModel userModel2 = UserModel.builder().firstName("Peter").isActive(false).lastName("Fake").userId("frrgd")
				.build();
		
		RolModel rol3 = RolModel.builder().rolId("3").description("Rol 3").user(userModel2).build();
		RolModel rol4 = RolModel.builder().rolId("4").description("Rol 4").user(userModel2).build();
		List<RolModel> rolList2 = new  ArrayList<>();
		rolList2.add(rol3);
		rolList2.add(rol4);
		userModel2.setRolList(rolList2);
		
		UserModel userModel3 = UserModel.builder().firstName("undefined").isActive(true).lastName("undefine")
				.userId("abcd").build();
		
		RolModel rol5 = RolModel.builder().rolId("5").description("Rol 5").user(userModel3).build();
		List<RolModel> rolList3 = new  ArrayList<>();
		rolList3.add(rol5);
		userModel3.setRolList(rolList3);
		
		List<UserModel> listModelUser = new ArrayList<>();
		listModelUser.add(userModel1);
		listModelUser.add(userModel2);
		listModelUser.add(userModel3);

		return listModelUser;
	}

	@Override
	public List<UserModel> getAllUserModel() {
		return getListUserModel();
	}

}
