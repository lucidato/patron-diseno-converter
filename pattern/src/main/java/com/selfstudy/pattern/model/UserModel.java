package com.selfstudy.pattern.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class UserModel implements Serializable {

	private static final long serialVersionUID = -7255195929219260268L;
	
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String userId;
	private List<RolModel> rolList;

}
