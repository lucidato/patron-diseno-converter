package com.selfstudy.pattern.converter;

import com.selfstudy.pattern.dto.RolDto;
import com.selfstudy.pattern.model.RolModel;

public class RolConverterAbstract implements AbstractConverter<RolModel, RolDto> {


	/**
	 * Convertir lista modelo a lista dto
	 */
	@Override
	public RolDto fromEntity(RolModel entity) {

		RolDto rol= new RolDto();
		rol.setDescription(entity.getDescription());
		rol.setRolId(entity.getRolId());
		 
		return rol;
	}

	@Override
	public RolModel fromDto(RolDto dto) {
	 
		return null;
	}

 
	 

}
