package com.selfstudy.pattern.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public interface AbstractConverter<E,D> {

	public abstract E fromDto(D dto);
	
	public abstract D fromEntity(E entity);
	
	public default List<E> fromDto(List<D> dtos){
		if(CollectionUtils.isEmpty(dtos))  
			return  new ArrayList<>(); // tambien se puede por excepcion 
		// return dtos.stream().map(dto -> fromDto(dto)).collect(Collectors.toList()); // equivale a la de abajo
		return dtos.stream().map(this::fromDto).collect(Collectors.toList());
	}
	
	public default List<D> fromEntity(List<E> entities){
		if(CollectionUtils.isEmpty(entities))return new ArrayList<>(); // tambien se puede por excepcion
		// return entities.stream().map(entity -> fromEntity(entity)).collect(Collectors.toList()); // equivalente al de abajo
		return entities.stream().map(this::fromEntity).collect(Collectors.toList());
	}
}

//  https://www.javaguides.net/2018/08/converter-design-pattern-in-java.html#:~:text=The%20Converter%20Design%20Pattern%20is%20a%20behavioral%20design%20pattern%20which,collection%20of%20objects%20between%20types.
// https://java-design-patterns.com/patterns/converter/
