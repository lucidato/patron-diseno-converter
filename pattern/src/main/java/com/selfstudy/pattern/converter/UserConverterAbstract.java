package com.selfstudy.pattern.converter;

import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.UserModel;

public class UserConverterAbstract implements AbstractConverter<UserModel, UserDto> {


	
	@Override
	public UserDto fromEntity(UserModel entity) {

		UserDto user = new UserDto();
		BeanUtils.copyProperties(entity,user);
//		user.setFirstName(entity.getFirstName());
//		user.setLastName(entity.getLastName());
//		user.setActive(entity.isActive());
//		user.setUserId(entity.getUserId());
//		
		if (entity.getRolList() != null) {
			user.setRolList(entity.getRolList().stream().map(rol -> new RolConverterAbstract().fromEntity(rol))
					.collect(Collectors.toList()));
		}
		return user;
	}

	@Override
	public UserModel fromDto(UserDto dto) {
	 
		return null;
	}

	 

}
