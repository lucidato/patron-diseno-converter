package com.selfstudy.pattern.converter;

import com.selfstudy.pattern.dto.UserDto;
import com.selfstudy.pattern.model.UserModel;

public class UserConverterConcrete extends ConverterConcrete <UserDto, UserModel> {

	  public UserConverterConcrete() {
	    super(UserConverterConcrete::convertToEntity, UserConverterConcrete::convertToDto);
	  }

	  private static UserDto convertToDto(UserModel user) {
	    return new UserDto(user.getFirstName(), user.getLastName(), user.isActive(), user.getUserId(), null);
	  }

	  private static UserModel convertToEntity(UserDto dto) {
	    return new UserModel(dto.getFirstName(), dto.getLastName(), dto.isActive(), dto.getUserId(), null );
	  }

}
