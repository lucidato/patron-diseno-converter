package com.selfstudy.pattern.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@ToString
public class UserDto implements Serializable {

	private static final long serialVersionUID = 8103213915751153349L;
	
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String userId;
	private List<RolDto> rolList;
}
