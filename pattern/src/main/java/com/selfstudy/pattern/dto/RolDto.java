package com.selfstudy.pattern.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class RolDto implements Serializable {

	private static final long serialVersionUID = 7772886616607758489L;
	private String description;
	private String rolId;
	@JsonIgnore
	private UserDto user;

}
